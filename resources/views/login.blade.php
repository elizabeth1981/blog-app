@extends('layouts.layout-login')

@section('body')
<form>
<div id="form column" class="container p-4 mt-5">
    <h1> LOGIN PLAYER </h1>
    <div class="p-5">
        <label for="" class="col-6 pb-2"> Email </label>
        <input type="email" class="my-2">
        <label for="" class="col-6 pb-2"> Password </label>
        <input type="password" class="my-2">
        <div class="row mt-3"></div>
        <input type="submit" value="Login" class="btn btn-primary col-4 mx-2">
        <a href="/newUser" type="submit" class="btn btn-succes col-4 mx-2"> Sign in </a>
    </div>
</div>
</form>
@endsection
