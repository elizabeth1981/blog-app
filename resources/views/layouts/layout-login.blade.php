<!DOCTYPE html>
<html lang="{{ str_replace('_','-', app() ->getLocale())}}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Laravel</title>
    <!--fonts -->
    <style>
        body {
            margin: 0;
            padding: 0;
            font-family: 'Roboto', sans-serif;
            text-align: center;
        }
        /* Input forms*/
        input[type="submit"]{
            padding: 10px;
            color: #fff;
            background: #0098cb;
            width: 320px;
            margin: 20px auto;
            border: 0;
            border-radius: 3px;
            cursor: pointer;
        }
        input[type="submit"]:hover{
            background-color: #00b8eb;
        }
        /* Header*/
        header{
        border-bottom: 2px solid #eee;
        padding: 20px 0;
        margin-bottom: 10px;
        width: %100;
        text-aling: center;
    }
    header a{
        text-decoration: none;
        color: #333;
    }

    </style>
</head>

<body class="antialiased">
    @yield('body')
</body>
</html>
